<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<script src="<c:url value="/assets/js/jquery-1.10.2.min.js" />"></script>
<script src="<c:url value="/assets/js/jquery-migrate-1.2.1.min.js" />"></script>
<script src="<c:url value="/assets/js/jquery-ui-1.10.3.min.js" />"></script>
<script src="<c:url value="/assets/js/bootstrap.min.js" />"></script>
<script src="<c:url value="/assets/js/modernizr.min.js" />"></script>
<script src="<c:url value="/assets/js/jquery.sparkline.min.js" />"></script>
<script src="<c:url value="/assets/js/toggles.min.js" />"></script>
<script src="<c:url value="/assets/js/retina.min.js" />"></script>
<script src="<c:url value="/assets/js/jquery.cookies.js" />"></script>
<script src="<c:url value="/assets/bower_components/sweetalert/lib/sweet-alert.min.js" />"></script>
<script src="<c:url value="/assets/js/jquery.datatables.min.js" />"></script>

<script src="<c:url value="/assets/js/custom.js" />"></script>


<c:if test="${not empty paramValues.javascripts}">
	<c:forEach var="javascript" items="${paramValues.javascripts}">
		<script src="<c:url value="${javascript}" />"></script>
	</c:forEach>
</c:if>



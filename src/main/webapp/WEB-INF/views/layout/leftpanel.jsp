<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
    <%@ taglib prefix="sec"
	uri="http://www.springframework.org/security/tags"%>
  <div class="leftpanel">
    
    <div class="logopanel">
        <h1><span>[</span> Go Formation <span>]</span></h1>
    </div><!-- logopanel -->
    
    <div class="leftpanelinner">
    
        
        <!-- This is only visible to small devices -->
        <div class="visible-xs hidden-sm hidden-md hidden-lg">   
            <div class="media userlogged">
                <img alt="" src="<c:url value="/assets/images/photos/loggeduser.png" />" class="media-object">
                <div class="media-body">
                    <h4><sec:authentication property="principal.username" /></h4>
                    <span>"Life is so..."</span>
                </div>
            </div>
          
            <h5 class="sidebartitle actitle">Mon Compte</h5>
            <ul class="nav nav-pills nav-stacked nav-bracket mb30">
              <li><a href="<c:url value="/compte/parametrage" />"><i class="fa fa-cog"></i> <span>Paramètres</span></a></li>
              <li><a href="<c:url value="/help" />"><i class="fa fa-question-circle"></i> <span>Aide</span></a></li>
              <li><a href="<c:url value="/j_spring_security_logout"/>"><i class="fa fa-sign-out"></i> <span>Déconnexion</span></a></li>
            </ul>
        </div>
      
      <h5 class="sidebartitle">Navigation</h5>
      <ul class="nav nav-pills nav-stacked nav-bracket">
        <li><a href="<c:url value="/" />" id="menu_dashboard"><i class="fa fa-home"></i> <span>Dashboard</span></a></li>
        
        <sec:authorize access="hasAnyRole('ETUDIANT_LISTE_READ', 'ETUDIANT_INSCRIPTION_READ', 'ABSENCES_READ', 'RETARDS_READ', 'ETUDIANT_BULLETIN_READ', 'ETUDIANT_DOCUMENT_READ', 'ETUDIANT_EMPLOIE_READ')">
          <li class="nav-parent"><a href="#" id="menu_etudiants"><i class="fa fa-graduation-cap"></i> <span>Étudiants</span></a>
          <ul class="children">
          <sec:authorize access="hasRole('ETUDIANT_LISTE_READ')">
            <li id="menu_etudiants_etudiant"><a href="<c:url value="/etudiants/etudiant" />"><i class="fa fa-caret-right"></i> Etudiants</a></li>
          </sec:authorize>
          <sec:authorize access="hasRole('ETUDIANT_INSCRIPTION_READ')">
            <li id="menu_etudiants_inscription"><a href="<c:url value="/etudiants/inscription" />"><i class="fa fa-caret-right"></i> Inscription</a></li>
           </sec:authorize>
            <sec:authorize access="hasRole('ABSENCES_READ')">
            <li id="menu_etudiants_absences"><a href="<c:url value="/etudiants/absences" />"><i class="fa fa-caret-right"></i> Absences</a></li>
            </sec:authorize>
            <sec:authorize access="hasRole('RETARDS_READ')">
            <li id="menu_etudiants_retards"><a href="<c:url value="/etudiants/retards" />"><i class="fa fa-caret-right"></i> Retards</a></li>
            </sec:authorize>
            <sec:authorize access="hasRole('ETUDIANT_BULLETIN_READ')">
            <li><a href="form-wizards.html"><i class="fa fa-caret-right"></i> Bulletin</a></li>
            </sec:authorize>
            <sec:authorize access="hasRole('ETUDIANT_DOCUMENT_READ')">
            <li><a href="wysiwyg.html"><i class="fa fa-caret-right"></i> Documents</a></li>
            </sec:authorize>
            <sec:authorize access="hasRole('ETUDIANT_EMPLOIE_READ')">
            <li><a href="wysiwyg.html"><i class="fa fa-caret-right"></i> Emploi du temps</a></li>
            </sec:authorize>
          </ul>
        </li>
        </sec:authorize>
        <sec:authorize access="hasRole('VERSEMENT_READ')">
          <li><a href="index.html"><i class="fa fa-eur"></i> <span>Versement</span></a></li>
          </sec:authorize>
          <sec:authorize access="hasAnyRole('MATIERE_READ', 'MODULE_READ', 'CONTROLE_READ')">
          <li class="nav-parent"><a href="#" id="menu_module"><i class="fa fa-suitcase"></i> <span>Modules</span></a>
	          <ul class="children">
	          <sec:authorize access="hasRole('MODULE_READ')">
	            <li id="menu_module_modules"><a href="<c:url value="/module/modules" />"><i class="fa fa-caret-right"></i> Module</a></li>
            </sec:authorize>
            <sec:authorize access="hasRole('MATIERE_READ')">
	            <li id="menu_module_matieres"><a href="<c:url value="/module/matieres" />"><i class="fa fa-caret-right"></i> Matiere</a></li>
	          </sec:authorize>
	          <sec:authorize access="hasRole('CONTROLE_READ')">
	            <li><a href="typography.html"><i class="fa fa-caret-right"></i> Controles</a></li>
	            </sec:authorize>
	          </ul>
	       	</li>
        </sec:authorize>
        <sec:authorize access="hasAnyRole('PERSONNEL_READ', 'ENSEIGNANT_READ')">
        <li class="nav-parent"><a href="#" id="menu_employer"><i class="fa fa-users"></i> <span>Employees</span></a>
          <ul class="children">
            <sec:authorize access="hasRole('ENSEIGNANT_READ')"><li id="menu_employer_professeurs"><a href="<c:url value="/employees/enseignant" />"><i class="fa fa-caret-right"></i> Professeurs</a></li></sec:authorize>
            <sec:authorize access="hasRole('PERSONNEL_READ')"><li id="menu_employer_personnels"><a href="<c:url value="/employees/personnel" />"><i class="fa fa-caret-right"></i> Personnels</a></li></sec:authorize>
          </ul>
        </li>
        </sec:authorize>
        <sec:authorize access="hasRole('CAN_ACCESS_PARAMETRAGE')">
          <li class="nav-parent"><a href="#" id="menu_parametrage"><i class="fa fa-cogs"></i> <span>Paramètrage</span></a>
              <ul class="children">
                  <li id="menu_parametrage_annee"><a href="<c:url value="/parametrage/annee" />"><i class="fa fa-caret-right"></i> Années universitaires</a></li>
                  <li id="menu_parametrage_centres"><a href="<c:url value="/parametrage/centres" />"><i class="fa fa-caret-right"></i> Centres</a></li>
                  <li id="menu_parametrage_formations"><a href="<c:url value="/parametrage/formations" />"><i class="fa fa-caret-right"></i> Formations</a></li>
                  <li id="menu_parametrage_semestre"><a href="<c:url value="/parametrage/semestre" />"><i class="fa fa-caret-right"></i> Semestres</a></li>
                  <li id="menu_parametrage_niveaux"><a href="<c:url value="/parametrage/niveaux" />"><i class="fa fa-caret-right"></i> Niveaux / Fillières</a></li>
                  <li id="menu_parametrage_salles"><a href="<c:url value="/parametrage/salles" />"><i class="fa fa-caret-right"></i> Salles</a></li>
                  <li id="menu_parametrage_profils"><a href="<c:url value="/parametrage/profils" />"><i class="fa fa-caret-right"></i> Profils</a></li>
                  <li id="menu_parametrage_utilisateur"><a href="<c:url value="/parametrage/utilisateurs" />"><i class="fa fa-caret-right"></i> Utilisateurs</a></li>
                  <li id="menu_parametrage_journal"><a href="<c:url value="/parametrage/journalisation" />"><i class="fa fa-caret-right"></i> Journaux de logs</a></li>
              </ul>
          </li>
          </sec:authorize>
      </ul>
      
      <!--<div class="infosummary">
        <h5 class="sidebartitle">Information Summary</h5>    
        <ul>
            <li>
                <div class="datainfo">
                    <span class="text-muted">Daily Traffic</span>
                    <h4>630, 201</h4>
                </div>
                <div id="sidebar-chart" class="chart"></div>   
            </li>
            <li>
                <div class="datainfo">
                    <span class="text-muted">Average Users</span>
                    <h4>1, 332, 801</h4>
                </div>
                <div id="sidebar-chart2" class="chart"></div>   
            </li>
            <li>
                <div class="datainfo">
                    <span class="text-muted">Disk Usage</span>
                    <h4>82.2%</h4>
                </div>
                <div id="sidebar-chart3" class="chart"></div>   
            </li>
            <li>
                <div class="datainfo">
                    <span class="text-muted">CPU Usage</span>
                    <h4>140.05 - 32</h4>
                </div>
                <div id="sidebar-chart4" class="chart"></div>   
            </li>
            <li>
                <div class="datainfo">
                    <span class="text-muted">Memory Usage</span>
                    <h4>32.2%</h4>
                </div>
                <div id="sidebar-chart5" class="chart"></div>   
            </li>
        </ul>
      </div>-->  <!-- infosummary -->
      
    </div><!-- leftpanelinner -->
  </div><!-- leftpanel -->
  
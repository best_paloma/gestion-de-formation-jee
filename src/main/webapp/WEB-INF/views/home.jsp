<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="f"%>


<%! String menuActuel = "menu_dashboard";  %>


<jsp:include page="layout/header.jsp" />
<jsp:include page="layout/leftpanel.jsp" />
<jsp:include page="layout/topmenu.jsp" />


<div class="pageheader">
      <h2><i class="fa fa-home"></i> GoFormation <span>Tableau de bord</span></h2>
      <div class="breadcrumb-wrapper">
          <span class="label">Vous êtes ici:</span>
        <ol class="breadcrumb">
          <li><a href="<c:url value="/" />">GoFormation</a></li>
          <li class="active">Tableau de bord</li>
        </ol>
      </div>
    </div>

<div class="contentpanel">
    
    <div class="row">

        <div class="col-sm-6 col-md-3">
          <div class="panel panel-success panel-stat">
            <div class="panel-heading">

              <div class="stat">
                <div class="row">
                  <div class="col-xs-4">
                    <img src="<c:url value="/assets/images/is-user.png" />" alt="" />
                  </div>
                  <div class="col-xs-8">
                    <small class="stat-label">Visits Today</small>
                    <h1>900k+</h1>
                  </div>
                </div><!-- row -->

                <div class="mb15"></div>

                <div class="row">
                  <div class="col-xs-6">
                    <small class="stat-label">Pages / Visit</small>
                    <h4>7.80</h4>
                  </div>

                  <div class="col-xs-6">
                    <small class="stat-label">% New Visits</small>
                    <h4>76.43%</h4>
                  </div>
                </div><!-- row -->
              </div><!-- stat -->

            </div><!-- panel-heading -->
          </div><!-- panel -->
        </div><!-- col-sm-6 -->

        <div class="col-sm-6 col-md-3">
          <div class="panel panel-danger panel-stat">
            <div class="panel-heading">

              <div class="stat">
                <div class="row">
                  <div class="col-xs-4">
                    <img src="<c:url value="/assets/images/is-document.png" />" alt="" />
                  </div>
                  <div class="col-xs-8">
                    <small class="stat-label">% Unique Visitors</small>
                    <h1>54.40%</h1>
                  </div>
                </div><!-- row -->

                <div class="mb15"></div>

                <small class="stat-label">Avg. Visit Duration</small>
                <h4>01:80:22</h4>

              </div><!-- stat -->

            </div><!-- panel-heading -->
          </div><!-- panel -->
        </div><!-- col-sm-6 -->

        <div class="col-sm-6 col-md-3">
          <div class="panel panel-primary panel-stat">
            <div class="panel-heading">

              <div class="stat">
                <div class="row">
                  <div class="col-xs-4">
                    <img src="<c:url value="/assets/images/is-document.png" />" alt="" />
                  </div>
                  <div class="col-xs-8">
                    <small class="stat-label">Page Views</small>
                    <h1>300k+</h1>
                  </div>
                </div><!-- row -->

                <div class="mb15"></div>

                <small class="stat-label">% Bounce Rate</small>
                <h4>34.23%</h4>

              </div><!-- stat -->

            </div><!-- panel-heading -->
          </div><!-- panel -->
        </div><!-- col-sm-6 -->

        <div class="col-sm-6 col-md-3">
          <div class="panel panel-dark panel-stat">
            <div class="panel-heading">

              <div class="stat">
                <div class="row">
                  <div class="col-xs-4">
                    <img src="<c:url value="/assets/images/is-money.png" />" alt="" />
                  </div>
                  <div class="col-xs-8">
                    <small class="stat-label">Today's Earnings</small>
                    <h1>$655</h1>
                  </div>
                </div><!-- row -->

                <div class="mb15"></div>

                <div class="row">
                  <div class="col-xs-6">
                    <small class="stat-label">Last Week</small>
                    <h4>$32,322</h4>
                  </div>

                  <div class="col-xs-6">
                    <small class="stat-label">Last Month</small>
                    <h4>$503,000</h4>
                  </div>
                </div><!-- row -->

              </div><!-- stat -->

            </div><!-- panel-heading -->
          </div><!-- panel -->
        </div><!-- col-sm-6 -->
      </div><!-- row -->
      
      <div class="row">
        
        <div class="col-sm-6 col-md-8">
        
        
        <div class="panel panel-default">
            <div class="panel-body">
            <h5 class="subtitle mb5">Niveau d'absences / retards</h5>
            <p class="mb15">Duis autem vel eum iriure dolor in hendrerit in vulputate...</p>
            <div id="area-chart" class="body-chart"></div>
            </div><!-- panel-body -->
          </div><!-- panel -->
        
        
        </div> <!-- .col-sm-6 -->
        <div class="col-sm-6 col-md-4">
        
        <div class="panel panel-default">
            <div class="panel-body">
            <h5 class="subtitle mb5">Most Browser Used</h5>
            <p class="mb15">Duis autem vel eum iriure dolor in hendrerit in vulputate Lorem ipsum dolor sit amet, consectetur adipisicing elit. Harum officia repudiandae sunt unde rem deleniti.

</p>
            <div id="donut-chart2" class="ex-donut-chart"></div>
            </div><!-- panel-body -->
          </div><!-- panel -->
        
        
        
        </div> <!-- ./col-sm-6 -->
       </div>
       
       <div class="row">
       <div class="col-sm-6 col-md-4">
       
       <div class="panel panel-dark panel-alt widget-todo">
          <div class="panel-heading">
              <div class="panel-btns">
                <a href="" class="tooltips" data-toggle="tooltip" title="Settings"><i class="glyphicon glyphicon-cog"></i></a>
                <a href="" id="addnewtodo" class="tooltips" data-toggle="tooltip" title="Add New"><i class="glyphicon glyphicon-plus"></i></a>
              </div><!-- panel-btns -->
              <h3 class="panel-title">To-Do List Widget</h3>
            </div>
            <div class="panel-body nopadding">
              <ul class="todo-list">
                <li>
                    <div class="ckbox ckbox-success">
                        <input type="checkbox" value="1" id="washcar" />
                        <label for="washcar">Wash car in neighbors house</label>
                    </div>
                </li>
                <li>
                    <div class="ckbox ckbox-success">
                        <input type="checkbox" value="1" id="eatpizza" checked="checked" />
                        <label for="eatpizza">Find and eat pizza anywhere</label>
                    </div>
                </li>
                <li>
                    <div class="ckbox ckbox-success">
                        <input type="checkbox" value="1" id="washdish" checked="checked" />
                        <label for="washdish">Wash the dishes and map the floor</label>
                    </div>
                </li>
                <li>
                    <div class="ckbox ckbox-success">
                        <input type="checkbox" value="1" id="buyclothes" />
                        <label for="buyclothes">Buy some clothes</label>
                    </div>
                </li>
                <li>
                    <div class="ckbox ckbox-success">
                        <input type="checkbox" value="1" id="throw" checked="checked" />
                        <label for="throw">Throw the garbage</label>
                    </div>
                </li>
                <li>
                    <div class="ckbox ckbox-success">
                        <input type="checkbox" value="1" id="reply" />
                        <label for="reply">Reply all emails for this week</label>
                    </div>
                </li>
              </ul>
            </div><!-- panel-body -->
          </div><!-- panel -->
          
       
        
          </div>
          
          <div class="col-sm-6 col-md-4">
          <div class="panel panel-default panel-alt widget-messaging">
          <div class="panel-heading">
              <div class="panel-btns">
                <a href="#." class="panel-edit"><i class="fa fa-info"></i></a>
              </div><!-- panel-btns -->
              <h3 class="panel-title">Journal d'opérations</h3>
            </div>
            <div class="panel-body">
              <ul>
                <li>
                  <small class="pull-right">Dec 10</small>
                  <h4 class="sender">Jennier Lawrence</h4>
                  <small>Lorem ipsum dolor sit amet...</small>
                </li>
                <li>
                  <small class="pull-right">Dec 9</small>
                  <h4 class="sender">Marsha Mellow</h4>
                  <small>Lorem ipsum dolor sit amet...</small>
                </li>
                <li>
                  <small class="pull-right">Dec 9</small>
                  <h4 class="sender">Holly Golightly</h4>
                  <small>Lorem ipsum dolor sit amet...</small>
                </li>
                <li>
                  <small class="pull-right">Dec 9</small>
                  <h4 class="sender">Holly Golightly</h4>
                  <small>Lorem ipsum dolor sit amet...</small>
                </li>
                <li>
                  <small class="pull-right">Dec 9</small>
                  <h4 class="sender">Holly Golightly</h4>
                  <small>Lorem ipsum dolor sit amet...</small>
                </li>
              </ul>
            </div><!-- panel-body -->
          </div><!-- panel -->
          </div>
          
          <div class="col-sm-6 col-md-4">
	          <div class="row">
	            <div class="col-xs-6">
	              <div class="panel panel-warning panel-alt widget-today">
	                <div class="panel-heading text-center">
	                  <i class="fa fa-calendar-o"></i>
	                </div>
	                <div class="panel-body text-center">
	                  <h3 class="today">Fri, Dec 13</h3>
	                </div><!-- panel-body -->
	              </div><!-- panel -->
	            </div>
	
	            <div class="col-xs-6">
	              <div class="panel panel-danger panel-alt widget-time">
	                <div class="panel-heading text-center">
	                  <i class="glyphicon glyphicon-time"></i>
	                </div>
	                <div class="panel-body text-center">
	                  <h3 class="today">4:50AM PST</h3>
	                </div><!-- panel-body -->
	              </div><!-- panel -->
	            </div>
	          </div>
	          
	           <div class="panel panel-default widget-weather">
            <div class="panel-body">
              <div class="row">
                <div class="col-xs-6 temp text-center">
                  <h1>18 <span>&#12444;</span></h1>
                  <h5><i class="fa fa-map-marker"></i> &nbsp; San Francisco</h5>
                </div>
                <div class="col-xs-6 weather text-center">
                  <i class="fa fa-cloud weather-icon"></i>
                  <div class="pull-left"><i class="fa fa-umbrella"></i> 1.0mm</div>
                  <div class="pull-right"><i class="fa fa-flag"></i> 30mph</div>
                </div>
              </div>
            </div><!-- panel-body -->
          </div><!-- panel -->
          </div>
          
        </div>
        
      
      
 </div> <!-- ./contentpanel -->
    
    
<jsp:include page="layout/rightpanel.jsp" />
<jsp:include page="layout/footer.jsp" />

<script src="<c:url value="/assets/js/flot/jquery.flot.min.js" />"></script>
<script src="<c:url value="/assets/js/flot/jquery.flot.resize.min.js" />"></script>
<script src="<c:url value="/assets/js/flot/jquery.flot.spline.min.js" />"></script>
<script src="<c:url value="/assets/js/morris.min.js" />"></script>
<script src="<c:url value="/assets/js/raphael-2.1.0.min.js" />"></script>


<script>
  jQuery(document).ready(function() {
    jQuery("a#<%= menuActuel %>").parent("li").addClass("active");
    
    function showTooltip(x, y, contents) {
		jQuery('<div id="tooltip" class="tooltipflot">' + contents + '</div>').css( {
		  position: 'absolute',
		  display: 'none',
		  top: y + 5,
		  left: x + 5
		}).appendTo("body").fadeIn(200);
	}
    
 // Donut Chart
    var m1 = new Morris.Donut({
         element: 'donut-chart2',
         data: [
           {label: "Chrome", value: 30},
           {label: "Firefox", value: 20},
           {label: "Opera", value: 20},
           {label: "Safari", value: 20},
           {label: "Internet Explorer", value: 10}
         ],
         colors: ['#D9534F','#1CAF9A','#428BCA','#5BC0DE','#428BCA']
     });
 
 
    var m2 = new Morris.Area({
        // ID of the element in which to draw the chart.
        element: 'area-chart',
        // Chart data records -- each entry in this array corresponds to a point on
        // the chart.
        data: [
            { y: '2006', a: 30, b: 20 },
            { y: '2007', a: 75,  b: 25 },
            { y: '2008', a: 50,  b: 40 },
            { y: '2009', a: 75,  b: 15 },
            { y: '2010', a: 50,  b: 40 },
            { y: '2011', a: 75,  b: 65 },
            { y: '2012', a: 100, b: 10 }
        ],
        xkey: 'y',
        ykeys: ['a', 'b'],
        labels: ['Absences', 'Retards'],
        lineColors: ['#1CAF9A', '#F0AD4E'],
        lineWidth: '1px',
        fillOpacity: 0.8,
        smooth: false,
        hideHover: false
    });
    
    
  //Add New To-Do
    jQuery('#addnewtodo').click(function(){
        
        var todo = jQuery(this).closest('.panel').find('.todo-list');
        if(todo.find('.todo-form').length == 0) {
            todo.prepend('<li class="todo-form">'+
                         '<div class="row">'+
                            '<div class="col-sm-8">'+
                                '<input type="text" class="form-control" placeholder="Type something here..." />'+
                            '</div>'+
                            '<div class="col-sm-4">'+
                                '<button class="btn btn-primary btn-sm btn-block">Add To-Do</button>'+
                            '</div>'+
                         '</li>');
        }
        return false;
    });
    
  });
</script>

</body>
</html>
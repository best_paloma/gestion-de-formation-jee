package com.app.propertyeditors;

import java.beans.PropertyEditorSupport;

import com.app.model.Role;

public class RoleEditor extends PropertyEditorSupport {
	
	// Converts a String to a Role (when submitting form)
    @Override
    public void setAsText(String text) {
    	int id=Integer.valueOf(text);
    	Role c = new Role();
    	c.setId(id);
        this.setValue(c);
    }

    // Converts a Role to a String (when displaying form)
    @Override
    public String getAsText() {
    	Role c = (Role) this.getValue();
        return c.getName();
    }

}

package com.app.propertyeditors;

import java.beans.PropertyEditorSupport;

import com.app.model.Matiere;

public class MatiereEditor extends PropertyEditorSupport {
	
	// Converts a String to a Permission (when submitting form)
    @Override
    public void setAsText(String text) {
    	int id=Integer.valueOf(text);
    	Matiere c = new Matiere();
    	c.setId(id);
        this.setValue(c);
    }

    // Converts a Permission to a String (when displaying form)
    public String getAsText() {
    	if(getValue() == null){
    	    return null;
    	   }
    	Matiere c = (Matiere) this.getValue();
    	if (c != null)
            return c.getIntitule();
        return null;
    }

}

package com.app.propertyeditors;

import java.beans.PropertyEditorSupport;

import com.app.model.Semestre;

public class SemestreEditor extends PropertyEditorSupport {
	
	// Converts a String to a Permission (when submitting form)
    @Override
    public void setAsText(String text) {
    	int id=Integer.valueOf(text);
    	Semestre c = new Semestre();
    	c.setId(id);
        this.setValue(c);
    }

    // Converts a Permission to a String (when displaying form)
    public String getAsText() {
    	if(getValue() == null){
    	    return null;
    	   }
    	Semestre c = (Semestre) this.getValue();
    	if (c != null)
            return c.getIntitule();
        return null;
    }

}

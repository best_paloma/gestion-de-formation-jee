package com.app.controller;



import javassist.NotFoundException;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.app.model.Annee;
import com.app.model.Centre;
import com.app.model.Filiere;
import com.app.model.Formation;
import com.app.model.Module;
import com.app.model.Niveaux;
import com.app.model.Semestre;
import com.app.propertyeditors.AnneeEditor;
import com.app.propertyeditors.CentreEditor;
import com.app.propertyeditors.FiliereEditor;
import com.app.propertyeditors.FormationEditor;
import com.app.propertyeditors.NiveauEditor;
import com.app.propertyeditors.SemestreEditor;
import com.app.service.AnneeService;
import com.app.service.CentreService;
import com.app.service.FiliereService;
import com.app.service.FormationService;
import com.app.service.LogTrackerService;
import com.app.service.ModuleService;
import com.app.service.NiveauxService;
import com.app.service.SemestreService;

@Controller
public class ModuleController {
	@Autowired
	ModuleService ModuleSvc;
	@Autowired
	CentreService centreSvc;
	@Autowired
	AnneeService anneeSvc;
	@Autowired
	NiveauxService niveauSvc;
	@Autowired
	FormationService formationSvc;
	@Autowired
	FiliereService filiereSvc;
	@Autowired
	SemestreService semestreSvc;
	@InitBinder
    public void initBinder(WebDataBinder binder) {
        binder.registerCustomEditor(Centre.class, new CentreEditor());
        binder.registerCustomEditor(Annee.class, new AnneeEditor());
        binder.registerCustomEditor(Niveaux.class, new NiveauEditor());
        binder.registerCustomEditor(Filiere.class, new FiliereEditor());
        binder.registerCustomEditor(Formation.class, new FormationEditor());
        binder.registerCustomEditor(Semestre.class, new SemestreEditor());
    }
	
	
	@PreAuthorize("hasRole('MODULE_READ')")
	@RequestMapping(value = "/module/modules", method = RequestMethod.GET)
	public String index(Model model) 
	{
		//model.addAttribute("mode", "create");
		
		
		model.addAttribute("liste_centre", centreSvc.getAll());
	
		model.addAttribute("liste_annee", anneeSvc.getAll());
	
		model.addAttribute("liste_filiere", filiereSvc.getAll());
	
		model.addAttribute("liste_formation", formationSvc.getAll());
		
		model.addAttribute("liste_niveaux", niveauSvc.getAll());

		model.addAttribute("semestre", semestreSvc.getAll());
		model.addAttribute("moduleForm", new Module());
		model.addAttribute("liste_modules", ModuleSvc.getAll());
		
		return "/module/modules/liste";
	}
	
	
	@PreAuthorize("hasRole('MODULE_EDIT')")
	@RequestMapping(value = "/module/modules/ajouter", method = RequestMethod.GET)
	public String ajouter(Model model) 
	{
	
		model.addAttribute("moduleForm", new Module());
		model.addAttribute("centreForm", new Centre());
		model.addAttribute("liste_centre", centreSvc.getAll());
		model.addAttribute("anneeForm", new Annee());
		model.addAttribute("liste_annee", anneeSvc.getAll());
		model.addAttribute("filiereForm", new Filiere());
		model.addAttribute("liste_filiere", filiereSvc.getAll());
		model.addAttribute("formationForm", new Formation());
		model.addAttribute("liste_formation", formationSvc.getAll());
		model.addAttribute("niveauxForm", new Niveaux());
		model.addAttribute("liste_niveaux", niveauSvc.getAll());
		model.addAttribute("semestreForm", new Semestre());
		model.addAttribute("semestre", semestreSvc.getAll());
	
		return "module/modules/ajouter";
	}
	
	@PreAuthorize("hasRole('MODULE_EDIT')")
	@RequestMapping(value = "/module/modules/saveModule", method = RequestMethod.POST)
	public String saveModule(
			@Valid @ModelAttribute("moduleForm") Module v,
			BindingResult bindingResult,
			@RequestParam(value = "mode", required = true, defaultValue = "create") String mode,
			Model model,
			final RedirectAttributes redirectAttributes
			) throws Exception {
		if (bindingResult.hasErrors()) {
			System.out.println("Erreur");
			System.out.println(bindingResult.toString());
			model.addAttribute("moduleForm", v);
			model.addAttribute("centreForm", new Centre());
			model.addAttribute("liste_centre", centreSvc.getAll());
			model.addAttribute("anneeForm", new Annee());
			model.addAttribute("liste_annee", anneeSvc.getAll());
			model.addAttribute("filiereForm", new Filiere());
			model.addAttribute("liste_filiere", filiereSvc.getAll());
			model.addAttribute("formationForm", new Formation());
			model.addAttribute("liste_formation", formationSvc.getAll());
			model.addAttribute("niveauxForm", new Niveaux());
			model.addAttribute("liste_niveaux", niveauSvc.getAll());
			model.addAttribute("semestreForm", new Semestre());
			model.addAttribute("semestre", semestreSvc.getAll());
			
			return "module/modules/ajouter";
		} else {
			
			if(mode.equals("create"))
			{
				ModuleSvc.add(v);
				redirectAttributes.addFlashAttribute("success_module", v.getIntitule() + " a ete bien ajouter");
				return "redirect:/module/modules/ajouter";
			}
			else{
				System.out.println("Updating "+v);
				try {
					ModuleSvc.update(v);
					redirectAttributes.addFlashAttribute("success_module", v.getIntitule() + " a ete bien modifier");
					return "redirect:/module/modules/ajouter";
				} catch (NotFoundException e) {
					redirectAttributes.addFlashAttribute("error_module", "Entity not found");
					return "redirect:/module/modules/ajouter";
				}
				
			}
			
		}
	}
	
	@PreAuthorize("hasRole('MODULE_READ')")
	@RequestMapping(value = "/module/modules/editModule", method = RequestMethod.GET)
	public String editModule(
			@RequestParam(value = "id", required = true, defaultValue = "0") int id,
			Model model
			) {
		Module p = ModuleSvc.findById(id);
		System.out.println("Editing "+p);
		model.addAttribute("moduleForm", p);
		model.addAttribute("centreForm", new Centre());
		model.addAttribute("liste_centre", centreSvc.getAll());
		model.addAttribute("anneeForm", new Annee());
		model.addAttribute("liste_annee", anneeSvc.getAll());
		model.addAttribute("filiereForm", new Filiere());
		model.addAttribute("liste_filiere", filiereSvc.getAll());
		model.addAttribute("formationForm", new Formation());
		model.addAttribute("liste_formation", formationSvc.getAll());
		model.addAttribute("niveauxForm", new Niveaux());
		model.addAttribute("liste_niveaux", niveauSvc.getAll());
		model.addAttribute("semestreForm", new Semestre());
		model.addAttribute("semestre", semestreSvc.getAll());
		
		return "module/modules/modifier";
	}

	@PreAuthorize("hasRole('MODULE_EDIT')")
	@RequestMapping(value = "/module/modules/updatemodule", method = RequestMethod.POST)
	public  String updatemodule(
			@Valid @ModelAttribute("moduleForm") Module p,
			BindingResult bindingResult,
			Model model,
			final RedirectAttributes redirectAttributes,
			HttpServletRequest request
			) throws Exception {
		if (bindingResult.hasErrors()) {
			//System.out.println("Erreur");
			System.out.println(bindingResult.toString());
			model.addAttribute("moduleForm", p);
			model.addAttribute("centreForm", new Centre());
			model.addAttribute("liste_centre", centreSvc.getAll());
			model.addAttribute("anneeForm", new Annee());
			model.addAttribute("liste_annee", anneeSvc.getAll());
			model.addAttribute("filiereForm", new Filiere());
			model.addAttribute("liste_filiere", filiereSvc.getAll());
			model.addAttribute("formationForm", new Formation());
			model.addAttribute("liste_formation", formationSvc.getAll());
			model.addAttribute("niveauxForm", new Niveaux());
			model.addAttribute("liste_niveaux", niveauSvc.getAll());
			model.addAttribute("semestreForm", new Semestre());
			model.addAttribute("semestre", semestreSvc.getAll());
		
		
			
			return "modules/modules";
		} else {
			
			try {
				ModuleSvc.update(p);
				
				redirectAttributes.addFlashAttribute("success_module", p.getIntitule() + " a ete bien modifier");
				return "redirect:/module/modules/editModule?id="+p.getId();
			} catch (NotFoundException e) {
				redirectAttributes.addFlashAttribute("error_module", "Entity not found");
				return "redirect:/module/modules/editModule?id="+p.getId();
			}
			
		}
	}
	
	@PreAuthorize("hasRole('MODULE_EDIT')")
	@RequestMapping(value = "/module/modules/deleteModule", method = RequestMethod.GET)
	public String deleteModule(
			@RequestParam(value = "id", required = true, defaultValue = "0") int id,
			final RedirectAttributes redirectAttributes,
			HttpServletRequest request
			) {
		try {
			Module p = ModuleSvc.findById(id);
			ModuleSvc.delete(id);
			

			redirectAttributes.addFlashAttribute("success_person_delete", p.getIntitule()
					+ " a ete supprimer");

			return "redirect:/module/modules";
		} catch (NotFoundException e) {
			redirectAttributes.addFlashAttribute("error_module_delete", "Entity not found");
			return "redirect:/module/modules";
		}
		
	}
	

}

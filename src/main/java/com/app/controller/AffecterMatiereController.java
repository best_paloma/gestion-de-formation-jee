package com.app.controller;

import javassist.NotFoundException;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.app.model.AffecterMatiere;
import com.app.model.Annee;
import com.app.model.Enseignant;
import com.app.model.Filiere;
import com.app.model.Formation;
import com.app.model.Matiere;
import com.app.model.Module;
import com.app.model.Niveaux;
import com.app.model.Semestre;
import com.app.propertyeditors.AnneeEditor;
import com.app.propertyeditors.EnseignantEditor;
import com.app.propertyeditors.FiliereEditor;
import com.app.propertyeditors.FormationEditor;
import com.app.propertyeditors.MatiereEditor;
import com.app.propertyeditors.ModuleEditor;
import com.app.propertyeditors.NiveauEditor;
import com.app.propertyeditors.SemestreEditor;
import com.app.service.AffecterMatiereService;
import com.app.service.AnneeService;
import com.app.service.EnseignantService;
import com.app.service.FiliereService;
import com.app.service.FormationService;
import com.app.service.MatiereService;
import com.app.service.ModuleService;
import com.app.service.NiveauxService;
import com.app.service.SemestreService;

@Controller
public class AffecterMatiereController {

	@Autowired
	AffecterMatiereService	affecterMatiereService;
	
	@Autowired
	AnneeService anneeService;

	@Autowired
	SemestreService semestreSvc;
	@Autowired
	FormationService formationSvc;

	@Autowired
	FiliereService filiereService;
	@Autowired
	NiveauxService niveauxService;
	@Autowired
	ModuleService moduleService;
	@Autowired
	MatiereService matiereService;
	@Autowired
	EnseignantService EnseignantService;
	
	
	
	
	@InitBinder
    public void initBinder(WebDataBinder binder) {
        binder.registerCustomEditor(Annee.class, new AnneeEditor());
        binder.registerCustomEditor(Formation.class, new FormationEditor());
        binder.registerCustomEditor(Filiere.class, new FiliereEditor());
        binder.registerCustomEditor(Niveaux.class, new NiveauEditor());
        binder.registerCustomEditor(Semestre.class, new SemestreEditor());
        binder.registerCustomEditor(Module.class, new ModuleEditor());
        binder.registerCustomEditor(Matiere.class, new MatiereEditor());
        binder.registerCustomEditor(Enseignant.class, new EnseignantEditor());




    }
	
	@PreAuthorize("hasRole('CAN_ACCESS_PARAMETRAGE')")
	@RequestMapping(value = "/module/affecterMatiere", method = RequestMethod.GET)
	public String index(Model model) {
		model.addAttribute("mode", "create");
		
		model.addAttribute("affecterForm", new AffecterMatiere());
		model.addAttribute("liste_affectations", affecterMatiereService.getAll());
		
		model.addAttribute("annees", anneeService.getAll());
		model.addAttribute("formations", formationSvc.getAll());
		model.addAttribute("filieres", filiereService.getAll());
		model.addAttribute("niveaux", niveauxService.getAll());
		model.addAttribute("semestre", semestreSvc.getAll());
		model.addAttribute("module", moduleService.getAll());
		model.addAttribute("matieres", matiereService.getAll());
		model.addAttribute("enseignant", EnseignantService.getAll());


		
		return "module/affecterMatiere";
	}
	
	@PreAuthorize("hasRole('CAN_ACCESS_PARAMETRAGE')")
	@RequestMapping(value = "/module/saveAffectation", method = RequestMethod.POST)
	public String saveAffectation(
			@Valid @ModelAttribute("affecterForm") AffecterMatiere v,
			BindingResult bindingResult,
			@RequestParam(value = "mode", required = true, defaultValue = "create") String mode,
			Model model,
			final RedirectAttributes redirectAttributes
			) throws Exception {
		if (bindingResult.hasErrors()) {
			System.out.println(bindingResult.toString());
			
			model.addAttribute("affecterForm", new AffecterMatiere());
			model.addAttribute("liste_affectations", affecterMatiereService.getAll());
			
			model.addAttribute("annees", anneeService.getAll());
			model.addAttribute("formations", formationSvc.getAll());
			model.addAttribute("filieres", filiereService.getAll());
			model.addAttribute("niveaux", niveauxService.getAll());
			model.addAttribute("semestre", semestreSvc.getAll());
			model.addAttribute("module", moduleService.getAll());
			model.addAttribute("matieres", matiereService.getAll());
			model.addAttribute("enseignant", EnseignantService.getAll());
			
			
			return "module/affecterMatiere";
		} else {
			
			if(mode.equals("create"))
			{
				affecterMatiereService.add(v);
				redirectAttributes.addFlashAttribute("success_affecter",   " a ete bien ajouter");
				return "redirect:/module/affecterMatiere";
			}
			else{
				System.out.println("Updating "+v);
				try {
					affecterMatiereService.update(v);
					redirectAttributes.addFlashAttribute("success_affecter", " a ete bien modifier");
					return "redirect:/module/affecterMatiere";
				} catch (NotFoundException e) {
					redirectAttributes.addFlashAttribute("error_affecter", "Entity not found");
					return "redirect:/module/affecterMatiere";
				}
				
			}
			
		}
	}
	

	
	@PreAuthorize("hasRole('CAN_ACCESS_PARAMETRAGE')")
	@RequestMapping(value = "/module/editAffecter", method = RequestMethod.GET)
	public String editAffecter(
			@RequestParam(value = "id", required = true, defaultValue = "0") int id,
			Model model
			) {
		AffecterMatiere p = affecterMatiereService.findById(id);
		System.out.println("Editing "+p);
		model.addAttribute("mode", "edit");
		
		model.addAttribute("affecterForm", p);
		model.addAttribute("/*Form", new AffecterMatiere());
		
		model.addAttribute("liste_affectations", affecterMatiereService.getAll());
		
		model.addAttribute("annees", anneeService.getAll());
		model.addAttribute("formations", formationSvc.getAll());
		model.addAttribute("filieres", filiereService.getAll());
		model.addAttribute("niveaux", niveauxService.getAll());
		model.addAttribute("semestre", semestreSvc.getAll());
		model.addAttribute("module", moduleService.getAll());
		model.addAttribute("matieres", matiereService.getAll());
		model.addAttribute("enseignant", EnseignantService.getAll());
		
		
		return "module/affecterMatiere";
	}
	

	@PreAuthorize("hasRole('CAN_ACCESS_PARAMETRAGE')")
	@RequestMapping(value = "/module/deleteAffecter", method = RequestMethod.GET)
	public String deleteAffecter(
			@RequestParam(value = "id", required = true, defaultValue = "0") int id,
			final RedirectAttributes redirectAttributes) {
		try {
			AffecterMatiere p = affecterMatiereService.findById(id);
			affecterMatiereService.delete(id);

			redirectAttributes.addFlashAttribute("success_affecter_delete", p.getMatiere()
					+ " a ete supprimer");

			return "redirect:/module/affecterMatiere";
		} catch (NotFoundException e) {
			redirectAttributes.addFlashAttribute("error_affecter_delete", "Entity not found");
			return "redirect:/module/affecterMatiere";
		}
		
	}
	
	

	
}

package com.app.controller;


import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.app.model.Etudiant;


@Controller
public class HomeController {


	@RequestMapping(value = "/", method = RequestMethod.GET)
	public ModelAndView home(HttpServletRequest request) {
		HttpSession session=request.getSession();
		if(session.getAttribute("etudiant_connecte") != null)
		{
			System.out.println((Etudiant) session.getAttribute("etudiant_connecte")); // null
		}
		
		ModelAndView mv = new ModelAndView("home");
		return mv;
	}

}
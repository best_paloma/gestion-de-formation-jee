package com.app.service;

import java.util.List;

import javassist.NotFoundException;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.transaction.annotation.Transactional;

import com.app.model.Filiere;

public class FiliereService {
	@PersistenceContext
	private EntityManager em;

	@Transactional
	public List<Filiere> getAll() {
		List<Filiere> result = em.createQuery("SELECT p FROM Filiere p",
				Filiere.class).getResultList();
		return result;
	}

	@Transactional
	public void add(Filiere p) {
		System.out.println("***** SERVICE *****");
		System.out.println(p);
		System.out.println("***** /SERVICE *****");
		em.persist(p);
		


	}
	
	@Transactional(rollbackFor=NotFoundException.class)
	public void delete(int id) throws NotFoundException {
		Filiere p = findById(id);
		if (p == null)
            throw new NotFoundException(null);
		
	   em.remove(p);
	}

	@Transactional(rollbackFor=NotFoundException.class)
	public Filiere update(Filiere p) throws NotFoundException{
		Filiere up = findById(p.getId());
		if (up == null)
            throw new NotFoundException(null);
		
		em.merge(p);
		return findById(p.getId());
	}

	public Filiere findById(int id) {
		return em.find(Filiere.class, id);
	}
}
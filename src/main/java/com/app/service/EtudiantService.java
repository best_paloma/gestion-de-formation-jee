package com.app.service;

import java.util.List;

import javassist.NotFoundException;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.transaction.annotation.Transactional;

import com.app.model.Etudiant;
import com.app.model.User;

public class EtudiantService {
	@PersistenceContext
	private EntityManager em;

	@Transactional
	public List<Etudiant> getAll() {
		List<Etudiant> result = em.createQuery("SELECT p FROM Etudiant p",
				Etudiant.class).getResultList();
		return result;
	}

	@Transactional
	public void add(Etudiant p) {
		System.out.println("***** SERVICE *****");
		System.out.println(p);
		System.out.println("***** /SERVICE *****");
		em.persist(p);
	}
	
	@Transactional(rollbackFor=NotFoundException.class)
	public void delete(int id) throws NotFoundException {
		Etudiant p = findById(id);
		if (p == null)
            throw new NotFoundException(null);
	   em.remove(p);
	}

	@Transactional(rollbackFor=NotFoundException.class)
	public Etudiant update(Etudiant p) throws NotFoundException{
		Etudiant up = findById(p.getId());
		if (up == null)
            throw new NotFoundException(null);
		
		em.merge(p);
		return findById(p.getId());
	}

	public Etudiant findById(int id) {
		return em.find(Etudiant.class, id);
	}
	
	@Transactional
	public Etudiant findByUser(User user)
            throws NotFoundException {
		List<Etudiant> result = em.createQuery("SELECT p FROM Etudiant p WHERE utilisateur = :user",
				Etudiant.class).setParameter("user", user).getResultList();
	    
	    return result.get(0);
	}
	
}
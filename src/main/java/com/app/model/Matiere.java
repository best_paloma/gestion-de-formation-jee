package com.app.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.Size;

import org.hibernate.validator.constraints.NotEmpty;

@Entity
@Table(name = "matiere")
public class Matiere {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="id_matiere")
	private int id;
	@NotEmpty(message = "Veuillez entrer un nom.")
	@Size(min = 4, max = 160, message = "Le nom doit etre compris entre 4 et 160 caracteres")
	private String intitule;
	
	
	private int coeff;
	
	@ManyToOne
	@JoinColumn(name="id_annee")
	private Annee annee;
	
	@ManyToOne
	@JoinColumn(name="id_centre")
	private Centre centre;
	
	@ManyToOne
	@JoinColumn(name="id_formation")
	private Formation formation;
	
	@ManyToOne
	@JoinColumn(name="id_filiere")
	private Filiere filiere;
	
	@ManyToOne
	@JoinColumn(name="id_niveaux")
	private Niveaux niveaux;
	
	@ManyToOne
	@JoinColumn(name="id_semestre")
	private Semestre semestre;

	
	@ManyToOne
	@JoinColumn(name="id_module")
	private Module module;

	@ManyToOne
	@JoinColumn(name="id_enseignant")
	private Enseignant enseignant;
	
	

	public Matiere() {
		super();
	}



	public Matiere(int id, String intitule, int coeff, Annee annee,
			Centre centre, Formation formation, Filiere filiere,
			Niveaux niveaux, Semestre semestre, Module module,
			Enseignant enseignant) {
		super();
		this.id = id;
		this.intitule = intitule;
		this.coeff = coeff;
		this.annee = annee;
		this.centre = centre;
		this.formation = formation;
		this.filiere = filiere;
		this.niveaux = niveaux;
		this.semestre = semestre;
		this.module = module;
		this.enseignant = enseignant;
	}



	public int getId() {
		return id;
	}



	public void setId(int id) {
		this.id = id;
	}



	public String getIntitule() {
		return intitule;
	}



	public void setIntitule(String intitule) {
		this.intitule = intitule;
	}



	public int getCoeff() {
		return coeff;
	}



	public void setCoeff(int coeff) {
		this.coeff = coeff;
	}



	public Annee getAnnee() {
		return annee;
	}



	public void setAnnee(Annee annee) {
		this.annee = annee;
	}



	public Centre getCentre() {
		return centre;
	}



	public void setCentre(Centre centre) {
		this.centre = centre;
	}



	public Formation getFormation() {
		return formation;
	}



	public void setFormation(Formation formation) {
		this.formation = formation;
	}



	public Filiere getFiliere() {
		return filiere;
	}



	public void setFiliere(Filiere filiere) {
		this.filiere = filiere;
	}



	public Niveaux getNiveaux() {
		return niveaux;
	}



	public void setNiveaux(Niveaux niveaux) {
		this.niveaux = niveaux;
	}



	public Semestre getSemestre() {
		return semestre;
	}



	public void setSemestre(Semestre semestre) {
		this.semestre = semestre;
	}



	public Module getModule() {
		return module;
	}



	public void setModule(Module module) {
		this.module = module;
	}



	public Enseignant getEnseignant() {
		return enseignant;
	}



	public void setEnseignant(Enseignant enseignant) {
		this.enseignant = enseignant;
	}



	@Override
	public String toString() {
		return intitule ;
	}
	
	
	
	
}

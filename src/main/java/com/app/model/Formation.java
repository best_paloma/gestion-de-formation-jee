package com.app.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.Size;

import org.hibernate.validator.constraints.NotEmpty;

@Entity
@Table(name = "formation")
public class Formation {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="id_formation")
	private int id;
	@NotEmpty(message = "Veuillez entrer un nom.")
	@Size(min = 4, max = 20, message = "Le nom doit etre compris entre 4 et 20 caracteres")
	private String intitule;
	
	@ManyToOne
	@JoinColumn(name="id_annee")
	private Annee annee;
	
	@ManyToOne
	@JoinColumn(name="id_centre")
	private Centre centre;
	

	public Centre getCentre() {
		return centre;
	}


	public void setCentre(Centre centre) {
		this.centre = centre;
	}


	public Annee getAnnee() {
		return annee;
	}


	public void setAnnee(Annee annee) {
		this.annee = annee;
	}


	public Formation() {
		super();
	}


	public Formation(int id, String intitule) {
		super();
		this.id = id;
		this.intitule = intitule;
	}


	public int getId() {
		return id;
	}


	public void setId(int id) {
		this.id = id;
	}


	public String getIntitule() {
		return intitule;
	}


	public void setIntitule(String intitule) {
		this.intitule = intitule;
	}
	
	


	@Override
	public String toString() {
		return intitule;
	}
	
	
	
	
	

}

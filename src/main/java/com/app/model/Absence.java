package com.app.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.Type;
import org.joda.time.DateTime;
import org.springframework.format.annotation.DateTimeFormat;

import com.app.enums.AbsenceType;
import com.app.enums.TypeSeance;

@Entity
@Table(name = "absence")
public class Absence {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="id_niveaux")
	private int id;
	
	@ManyToOne
	@JoinColumn(name="id_filiere")
	private Filiere filiere;
	
	@ManyToOne
	@JoinColumn(name="id_annee")
	private Annee annee;
	
	@ManyToOne
	@JoinColumn(name="id_centre")
	private Centre centre;
	
	@ManyToOne
	@JoinColumn(name="id_formation")
	private Formation formation;
	
	@ManyToOne
	@JoinColumn(name="id_niveau")
	private Niveaux niveau;
	
	@ManyToOne
	@JoinColumn(name="id_module")
	private Module module;
	
	@ManyToOne
	@JoinColumn(name="id_matiere")
	private Matiere matiere;
	
	@ManyToOne
	@JoinColumn(name="id_etudiant")
	private Etudiant etudiant;
	
	@ManyToOne
	@JoinColumn(name="id_semestre")
	private Semestre semestre;
	
	@Column(name="type")
	@Enumerated(EnumType.ORDINAL)
	private AbsenceType type;
	
	@Column(name="type_seance")
	@Enumerated(EnumType.ORDINAL)
	private TypeSeance typeSeance;
	
	@Column(name="date_seance")
	@Type(type="org.jadira.usertype.dateandtime.joda.PersistentDateTime")
	@DateTimeFormat(pattern = "MM/dd/yyyy")
	private DateTime dateSeance;
	
	@Column(name="heure_debut")
	private String heureDebut;
	
	@Column(name="heure_fin")
	private String heureFin;
	
	private boolean justifier = false;
	private String justification = "";

	public Absence() {
		super();
		// TODO Auto-generated constructor stub
	}
	

	


	





	public Absence(int id, Filiere filiere, Annee annee, Centre centre,
			Formation formation, Niveaux niveau, Module module,
			Matiere matiere, Etudiant etudiant, Semestre semestre,
			AbsenceType type, TypeSeance typeSeance, DateTime dateSeance,
			String heureDebut, String heureFin, boolean justifier,
			String justification) {
		super();
		this.id = id;
		this.filiere = filiere;
		this.annee = annee;
		this.centre = centre;
		this.formation = formation;
		this.niveau = niveau;
		this.module = module;
		this.matiere = matiere;
		this.etudiant = etudiant;
		this.semestre = semestre;
		this.type = type;
		this.typeSeance = typeSeance;
		this.dateSeance = dateSeance;
		this.heureDebut = heureDebut;
		this.heureFin = heureFin;
		this.justifier = justifier;
		this.justification = justification;
	}











	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Filiere getFiliere() {
		return filiere;
	}

	public void setFiliere(Filiere filiere) {
		this.filiere = filiere;
	}

	public Annee getAnnee() {
		return annee;
	}

	public void setAnnee(Annee annee) {
		this.annee = annee;
	}

	public Centre getCentre() {
		return centre;
	}

	public void setCentre(Centre centre) {
		this.centre = centre;
	}

	public Formation getFormation() {
		return formation;
	}

	public void setFormation(Formation formation) {
		this.formation = formation;
	}

	public Niveaux getNiveau() {
		return niveau;
	}

	public void setNiveau(Niveaux niveau) {
		this.niveau = niveau;
	}

	

	public Module getModule() {
		return module;
	}











	public void setModule(Module module) {
		this.module = module;
	}











	public Matiere getMatiere() {
		return matiere;
	}

	public void setMatiere(Matiere matiere) {
		this.matiere = matiere;
	}

	public Etudiant getEtudiant() {
		return etudiant;
	}

	public void setEtudiant(Etudiant etudiant) {
		this.etudiant = etudiant;
	}
	
	

	public Semestre getSemestre() {
		return semestre;
	}



	public void setSemestre(Semestre semestre) {
		this.semestre = semestre;
	}



	public AbsenceType getType() {
		return type;
	}

	public void setType(AbsenceType type) {
		this.type = type;
	}

	public TypeSeance getTypeSeance() {
		return typeSeance;
	}

	public void setTypeSeance(TypeSeance typeSeance) {
		this.typeSeance = typeSeance;
	}

	public DateTime getDateSeance() {
		return dateSeance;
	}

	public void setDateSeance(DateTime dateSeance) {
		this.dateSeance = dateSeance;
	}

	public String getHeureDebut() {
		return heureDebut;
	}

	public void setHeureDebut(String heureDebut) {
		this.heureDebut = heureDebut;
	}

	public String getHeureFin() {
		return heureFin;
	}

	public void setHeureFin(String heureFin) {
		this.heureFin = heureFin;
	}
	
	

	public boolean isJustifier() {
		return justifier;
	}





	public void setJustifier(boolean justifier) {
		this.justifier = justifier;
	}





	public String getJustification() {
		return justification;
	}





	public void setJustification(String justification) {
		this.justification = justification;
	}





	@Override
	public String toString() {
		return ""+id;
	}
	
	
	
	
}

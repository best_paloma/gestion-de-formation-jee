package com.app.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.Size;

import org.hibernate.validator.constraints.NotEmpty;

@Entity
@Table(name = "salle")
public class Salle {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="id_salle")
	private int id;
	@NotEmpty(message = "Veuillez entrer un nom.")
	@Size(min = 2, max = 20, message = "Le nom doit etre compris entre 2 et 20 caracteres")
	private String intitule;
	
	@ManyToOne
	@JoinColumn(name="id_centre")
	private Centre centre;
	
	@ManyToOne
	@JoinColumn(name="id_annee")
	private Annee annee;
	public Salle() {
		super();
	}
	
	
	public Annee getAnnee() {
		return annee;
	}


	public void setAnnee(Annee annee) {
		this.annee = annee;
	}


	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getIntitule() {
		return intitule;
	}

	public void setIntitule(String intitule) {
		this.intitule = intitule;
	}

	public Centre getCentre() {
		return centre;
	}


	public void setCentre(Centre centre) {
		this.centre = centre;
	}

	public Salle(int id, String intitule, Centre centre) {
		super();
		this.id = id;
		this.intitule = intitule;
		this.centre = centre;
	}


	@Override
	public String toString() {
		return "Salle [id=" + id + ", intitule=" + intitule + ", centre="
				+ centre + "]";
	}



	
	
	
	
	
	
}
